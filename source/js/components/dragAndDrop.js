

/*
	Отвечает за перетаскивание(dragAndDrop система)
*/




// ondragover="allowDrow(event)"
// На каком элементе перетаскивается
function allowDragover(ev) {
	ev.preventDefault();
	ev.stopPropagation();
	console.log("allowDragover: тянем-потянем!");
}

function cardDrag(ev) {
	ev.stopPropagation();

	console.log("drag: Start схватить элемент");
	ev.dataTransfer.setData("parentId", ev.target.getAttribute("data-parent-id"));
	ev.dataTransfer.setData("id", ev.target.getAttribute("data-id"));
	ev.dataTransfer.setData("type", ev.target.getAttribute("data-type"));
	if (  ev.target.getAttribute("data-type") == app.cardsType.text ) {
		// text-content - текст из карточки
		ev.dataTransfer.setData( "text-content", ev.target.querySelector(".cart__text").innerHTML );
	}
	console.log("drag: End схватить элемент");
}


// drop event
// ev - событие
// block - Куда опускать элемент
function CardDrop(ev, block) {
	ev.stopPropagation();

	// logic
	// Переместтиь элемент на фронте
	// обновить app.state
	// Обновить state на сервере

	console.log("drop: start вставка элемента");

	// Получить все данные из элемента, которые перетаскиваем
	var cardParentId = ev.dataTransfer.getData("parentId");
	var cardId = ev.dataTransfer.getData("id");
	var cardType = ev.dataTransfer.getData("type");
	var cardText = ev.dataTransfer.getData("text-content");

	var data = ev.dataTransfer.getData("text");
	var content = ev.dataTransfer.getData("content");

	// Определить в какой блок мы перемещаем элемент
	// rasklad
	// card-wrapper
	// card__koloda

	var item
	if ( block.classList.contains('rasklad') ) {

		// Перемещаем в основную область

		// Меняем данные в app
		// Поиск и замена Элемента
		findElement( "rasklad", block.getAttribute("data-id"), cardType ,cardId, cardParentId);
		// Отрисовываем rasklad
		app.updateRasklad();


		var item = document.querySelector( ".card[data-parent-id='"+cardParentId+"'][data-id='"+cardId+"']");
		item.parentNode.removeChild(item);

		// Убрать элемент из слайдера
		var appItems = app.state.data.mutedCards.deck.items;
		appItems.forEach(function(item, i, arr){
			if (item.id == cardId) {
				arr.splice(i, 1);
			}
		});
	} else if ( block.classList.contains('card-wrapper') ) {

		// Перемещение в уже созданную карточку

		// 2 версия переноса из слайдера элемента
		// Поиск и замена Элемента
		findElement("card-wrapper",  block.getAttribute("data-id"), cardType, cardId, cardParentId);
		// Отрисовываем rasklad
		app.updateRasklad();

		var item = document.querySelector( ".card[data-parent-id='"+cardParentId+"'][data-id='"+cardId+"']");
		item.parentNode.removeChild(item);

		// Переместить элемент
		var appItems = app.state.data.mutedCards.deck.items;
		appItems.forEach(function(item, i, arr){
			if (item.id == cardId) {
				arr.splice(i, 1);
			}
		});
	} else if ( block.classList.contains('card__koloda') ) {

		// Перемещение за пределы рабочей области
		// Должны вернуть карточки на свои места

		var tempImg;
		var tempText;

		// Ищим текст или картинку в раскладе
		app.state.data.mutedCards.rasklad.forEach(function (item, i) {
			if ( item.id == cardId ) {
				if( Object.keys(item.data.cardText).length !== 0 ){
					returnElementState( item.data.cardText );
				}
				if (Object.keys(item.data.cardImg).length !== 0) {
					returnElementState( item.data.cardImg );
				}
				app.state.data.mutedCards.rasklad.splice(i, 1);
				toggleMode_update();
				app.updateRasklad();
			}
		});

		app.requestSetApp();
		return false;
	}
	app.requestSetApp();
}

// input:
//		el 	- Элемент, который нужно переместить
//	action: Переместить элементы, если нужно их вернуть на старые места
function returnElementState(el) {
	// Найти колоду карточки
	// Сначала ищим в слайдере
	if ( app.state.data.mutedCards.deck.id ==  el.parentId ) {
		app.state.data.mutedCards.deck.items.push( el );
	}

	// Ищим в оставшихся местах
	app.state.data.cards.forEach(function(item, i, arr){
		if ( el.parentId == item.id ) {
			item.items.push( el );
		}
	});
}

//	input:
//		plan 			- Место куда хотим добавить карточку
//		blockId			- id card-wrapper
//		elType			- Тип элемента(текст или картинка)
// 		cardId 			- id  карточки
//		cardParentId 	- id колоды
//	action: Ищет нужный элемент, вырезает из старого места и добавляет в rasklad (все в app)
//	output: true/false
function findElement(plan, blockId, elType, cardId, cardParentId) {
	var itemRasklad = {
		id: String(cardParentId) + String(cardId),
		data: {
			cardImg: {},
			cardText: {}
		}
	};

	if (plan == "rasklad") {
		console.log("вставить в rasklad");

		// Поиск по слайдеру(deck)
		app.state.data.mutedCards.deck.items.forEach(function(item, i){
			if ( item.parentId == cardParentId && item.id == cardId ) {
				if ( app.state.data.mutedCards.deck.type == app.cardsType.img ) {
					itemRasklad.data.cardImg = item;
				} else if ( app.state.data.mutedCards.deck.type == app.cardsType.text ) {
					itemRasklad.data.cardText = item;
				}
				app.state.data.mutedCards.rasklad.push( itemRasklad );
				console.log( "Нашли элемент в deck: ", itemRasklad );

				return true;
			}
		});

	} else if ( plan == "card-wrapper") {
		console.log("вставить в card");
		app.state.data.mutedCards.rasklad.forEach(function(el, i, arr) {
			if (el.id == blockId) {

				var temp;

				app.state.data.mutedCards.deck.items.forEach(function(item, j){
					if ( item.parentId == cardParentId && item.id == cardId ) {
						if ( app.state.data.mutedCards.deck.type == app.cardsType.img ) {
							temp = item;
						} else if ( app.state.data.mutedCards.deck.type == app.cardsType.text ) {
							temp = item;
						}
						console.log( "Нашли элемент в deck: ", itemRasklad );
					}
				});

				if (elType == app.cardsType.text) {
					el.data.cardText = temp;
				} else if ( elType == app.cardsType.img) {
					el.data.cardImg = temp;
				}

			}
		});
	}

	return false;
}
