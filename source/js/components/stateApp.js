
/*
	Singleton

	Глобальный объект с различными состояниями от приложения.
	Данные храняться также здесь.

	button.type 	- колода
	button.mode 	- тип колоды(открыта - true \ закрыта - false)
*/

var app = {
	// Типы колод
	cardsType: {
		text: 'text',
		img: 'img',
	},
	// состояние с сервера
	state: {},
	getCards: function () {
		console.log("Start(getCards) Обновляем картинки в колоде");

		// тип колоды(id колоды)
		var id 	= this.state.button.type;
		// mode колоды(открыта\закрыта)
		var mode 	= this.state.button.mode;

		var collectionWrapper = document.querySelector(".card__koloda");
		console.log("this.state.data.mutedCards.deck- ", this.state.data.mutedCards);
		var typeCollectionCards = this.state.data.mutedCards.deck.type;
		var collectionCards = this.state.data.mutedCards.deck.items;

		// Очистить блок с картами
		collectionWrapper.innerHTML = "";
		collectionCards.forEach(function (item, i, arr) {

			var card = document.createElement("div");
			card.classList.add("card");
			// Если закрыта, то добавляем .card_close
			if ( app.state.button.mode == false ) {
				card.classList.add("card_close");
				app.state.data.mutedCards.deck.items.forEach(function (el, j) {
					el.mode = false;
				});

			}
			card.setAttribute("draggable", true);
			card.setAttribute("ondragstart", "return cardDrag(event)");
			// card.setAttribute("data-name", "test 1");
			card.setAttribute("data-type", "img");
			card.setAttribute("data-type", typeCollectionCards);
			card.setAttribute("data-parent-id", item.parentId);
			card.setAttribute("data-id", item.id);

			var link = document.createElement("a");

			if ( typeCollectionCards == app.cardsType.text ) {

				// Вставляяем карточки с текстом
				var p = document.createElement("p");
				p.classList.add("card__description_full");
				p.classList.add("cart__text");
				p.innerHTML = item.val;
				link.appendChild(p);
			} else if ( typeCollectionCards == app.cardsType.img ) {

				// Вставляем карточку с картинкой
				var img = document.createElement("img");
				img.setAttribute("src", item.val);
				img.setAttribute("alt", "img");
				img.setAttribute("draggable", "false");
				link.appendChild(img);
			}

			card.appendChild(link);
			collectionWrapper.appendChild(card);
		});
		console.log("End(getCards) Обновляем картинки в колоде");
	},
	requestUpdatedApp: function () {
		// Получаем состояние с сервера
		$.ajax({
			type: "GET",
			url: "test.json",
			dataType: 'json',
			success: function(data){
				console.log(1);
				console.log("data - ", data);
				app.state = data;
				app.updateApp();

			},
			error: function (jqXHR, exception) {
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (jqXHR.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
				console.log(msg);
			}
		});
	},
	requestSetApp: function () {
		// Обновляем состояние на сервере
		// отправить app.state
		console.log("requestSetApp: Обновляем состояние на сервере");
	},

	// Для отладки! Начальное состояние программы с сервера
	getStateApp: function () {
		console.log("getStateApp: ", this.state);
	},

	// Для отладки! Текущее состояние программы с сервера
	q: function () {
		// Получаем состояние с сервера
		$.ajax({
			type: "GET",
			url: "testState.json",
			dataType: 'json',
			success: function(data){
				console.log("Получили что-то с сервера!");
				console.log("Воит это получили с сервера data - ", data);
				app.state = data;
				app.updateApp();
			}
		});
	},

	// Обновление frontend части всего приложения
	updateApp: function () {
		this.getStateApp();
		// Обновляем состояние кнопок
		app.updateRasklad();
		toggleMode_update();
		console.log("updateApp");
	},

	// Обновление rasklad
	updateRasklad: function () {
		var collection = app.state.data.mutedCards.rasklad;

		document.querySelector(".rasklad").innerHTML = "";

		collection.forEach(function (item, i, arr) {
			console.log(item);

			var wrapper = document.createElement("a");
			wrapper.classList.add("card-wrapper");

			wrapper.setAttribute("data-fancybox", "gallery");
			wrapper.setAttribute("href", item.data.cardImg.val );
			wrapper.setAttribute("draggable", true);
			wrapper.setAttribute("ondragstart", "cardDrag(event)");
			wrapper.setAttribute("data-id", item.id);
			wrapper.setAttribute("ondrop", "CardDrop(event, this)");
			wrapper.setAttribute("ondragover", "allowDragover(event)");
			wrapper.addEventListener("click",  function (e) {
				app.handlerClickCardWrapper(item, e);
			});
			wrapper.addEventListener("contextmenu",  function (e) {
				app.handlerClickCardWrapper(item, e);
			});

			var preview = document.createElement("div");
			preview.classList.add("card__preview");
			if ( Object.keys(item.data.cardText).length == 0 ) {
				// Нет текста
				wrapper.classList.add( "card_only-img" );

				var img = document.createElement("img");
				img.setAttribute("src", item.data.cardImg.val);
				img.setAttribute("alt", "img");
				img.setAttribute("draggable", false);

				if ( !item.data.cardImg.mode ) {
					wrapper.classList.add("card_close");
				}

				preview.appendChild( img );
			}

			var text = document.createElement("div");
			text.classList.add("card__text");
			if ( Object.keys(item.data.cardImg).length == 0  ) {
				// нет картинки
				wrapper.classList.add( "card_only-text" );

				var p = document.createElement("p");
				p.innerHTML = item.data.cardText.val;

				console.log("g 1 ",!item.data.cardText.mode);
				if ( !item.data.cardText.mode) {
					wrapper.classList.add("card_close");
				}

				text.appendChild( p );
			}

			if ( Object.keys(item.data.cardText).length !== 0 && Object.keys(item.data.cardImg).length !== 0) {
				var img = document.createElement("img");
				img.setAttribute("src", item.data.cardImg.val);
				img.setAttribute("alt", "img");
				img.setAttribute("draggable", false);
				preview.appendChild( img );

				var p = document.createElement("p");
				p.innerHTML = item.data.cardText.val;
				text.appendChild( p );
				if ( !item.data.cardImg.mode ) {
					wrapper.classList.add("card_close");
				}
				console.log("g 1 ",!item.data.cardText.mode);
				if ( !item.data.cardText.mode) {
					wrapper.classList.add("card_close");
				}
			}

			wrapper.appendChild( preview );
			wrapper.appendChild( text );

			document.querySelector(".rasklad").appendChild( wrapper );
		});

		$.fancybox.getInstance();
	},

	handlerClickCardWrapper: function (el, e) {

		if (e.which == 3) {
			e.preventDefault();
			e.stopPropagation();
			// Переворот
			if ( Object.keys(el.data.cardImg).length !== 0 ) {
				el.data.cardImg.mode = !el.data.cardImg.mode;
			}
			if ( Object.keys(el.data.cardText).length !== 0 ) {
				el.data.cardText.mode = !el.data.cardText.mode;
			}
			app.updateRasklad();
			app.requestSetApp();
		}

	}





};
