
/*
	Кнопки для переключения колод

	Клик по кнопке - загрузка изображений
	- на кнопку добавляется состояние(класс)
*/


var toggleMode_btnCollectionWrapper = document.querySelectorAll(".app__btn-collection-wrapper");
function toggleMode_update() {
	console.log(111111111111111111);
	// на основе состояния app включаем нужные кнопки
	var type = app.state.button.type;
	var mode = app.state.button.mode;

	// Убираем состояние кнопки
	var arrBtn = document.querySelectorAll(".app__btn-collection-wrapper .btn");
	// console.log( arrBtn );
	arrBtn.forEach(function(item, i)  {
		item.classList.remove("active");
	});

	// Задаем начальное состояние для кнопок
	toggleMode_btnCollectionWrapper.forEach(function(item, i, arr){
		if ( app.state.button.type == item.getAttribute("data-button-type") ) {
			var query = +app.state.button.mode ;

			var btn = item.querySelector(".btn[data-button-mode='" + query + "']");
			btn.classList.add("active");

			// загрузить картинки
			app.getCards();
		}
	});

	// ----- Start Для тестирования
	app.getStateApp();
	// ----- end Для тестирования
}

function toggleMode_init() {
	// Добавление событий на кнопки
	toggleMode_btnCollectionWrapper.forEach(function(item, i, arr){
		var collectionBtn = item.querySelectorAll('.btn');
		collectionBtn.forEach(function(deepItem, deepI, deepArr) {

			// биндим кнопки на изменение состояния
			deepItem.addEventListener("click", function () {
				var btnMode = Boolean( parseInt( this.getAttribute("data-button-mode") ) );

				// Меняем состояние кнопки у app
				app.state.button.mode = btnMode;

				var parentBtn = this.parentElement.parentElement;
				var btnType = parseInt( parentBtn.getAttribute("data-button-type") );

				// Меняем тип кнопки у app
				app.state.button.type  = btnType;

				// Убираем со всех кнопок класс active
				var arrBtn = document.querySelectorAll(".app__btn-collection-wrapper .btn");
				// console.log( arrBtn );
				arrBtn.forEach(function(item, i)  {
					item.classList.remove("active");
				});

				// Обновить колоду карт в слайдере
				var id 		= app.state.button.type;
				var mode 	= app.state.button.mode;

				var oldDeck = app.state.data.mutedCards.deck;
				var newDeck;

				// Определяем mode отображения
				if ( mode && id == app.state.data.mutedCards.deck.id ) {
					// меняем mode отображения в newDeck
					app.state.data.mutedCards.deck.items.forEach( function (item, i, arr) {
						item.mode = true;
					});
					console.log("Поменяли мод отображения в deck на true");
				} else if ( !mode && id == app.state.data.mutedCards.deck.id) {
					// меняем mode отображения в newDeck
					app.state.data.mutedCards.deck.items.forEach( function (item, i, arr) {
						item.mode = false;
					});
					console.log("Поменяли мод отображения в deck на false");
				} else if( id != app.state.data.mutedCards.deck.id ){
					// Ищим нужную колоду
					app.state.data.cards.forEach(function(item, i, arr) {
						console.log("item.id == id - ", item.id,  id)
						if ( item.id == id ) {

							// Перемещаем в кеш колоду, которая поподет в слайдер
							newDeck = item;
							// Удаляем колоду из массива
							app.state.data.cards.splice(i, 1);
						}
					});
					// Возвращаем колоду в основной массив
					app.state.data.cards.push( oldDeck );
					// Меняем актикную колоду в слайдере
					app.state.data.mutedCards.deck = newDeck ;
					console.log("Заменили колоду в deck");
				}


				// Обновить картинки в слайдере
				app.getCards();
				// Обновить состояние на сервере
				app.requestSetApp();

				this.classList.add("active");

				// ----- Start Для тестирования
				app.getStateApp();
				// ----- end Для тестирования
			});
		});
	});

	// ----- Start Для тестирования
	app.getStateApp();
	// ----- end Для тестирования
}
// 	// ----- Start Для тестирования
// 	app.getStateApp();
// 	// ----- end Для тестирования
// }
function toggleMode_loadTypeCollection(type, mode) {
	console.log("Загрзука коллекции с карточками", type, mode);
}
