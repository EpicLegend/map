

var slider;

var slider_state = {
	appAdd: function (array, mode, indexCollection) {
		console.log(".appAdd");
		// var classMode;
		// if ( parseInt(mode) ) {
		// 	classMode = "";
		// } else {
		// 	classMode = "app__slider__item_close";
		// }
		// array.forEach(function(item, index, arr) {
		// 	if (indexCollection == 4) {
		// 		$(".app__slider").slick('slickAdd', '<div class="app__slider__item app__slider__item_text'+classMode+'" data-item="'+ (index + 1) +'" data-mode-item="text"><div class="app__slider__item-container"> <p>'+ item +'</p> </div></div>');
		// 	} else {
		// 		$(".app__slider").slick('slickAdd', '<div class="app__slider__item '+classMode+'" data-item="'+ (index + 1) +'" data-mode-item="img"><img src="'+ item +'" alt="img"></div>');
		// 	}
		// });
	},
	appRemove: function () {
		slider.slick("slickRemove", null, null, true);
	}
}


function sliderInit(selector) {
	console.log("SliderInit start: ", selector);
	// slider = document.querySelector(selector).slick({
	// 	dots: false,
	// 	infinite: false,
	// 	speed: 300,
	// 	slidesToShow: 1,
	// 	centerMode: false,
	// 	variableWidth: true,
	// 	prevArrow: '<div class="app__slider__arrow__left"><svg width="16" height="29" viewBox="0 0 16 29" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.557225 25.6149C-0.207724 26.4118 -0.181886 27.6778 0.614937 28.4428C1.41176 29.2077 2.67783 29.1819 3.44277 28.3851L0.557225 25.6149ZM14 14.5L15.4428 15.8851C16.1857 15.1111 16.1857 13.8889 15.4428 13.1149L14 14.5ZM3.44277 0.614938C2.67782 -0.181886 1.41176 -0.207723 0.614935 0.557225C-0.181888 1.32218 -0.207726 2.58824 0.557223 3.38506L3.44277 0.614938ZM3.44277 28.3851L15.4428 15.8851L12.5572 13.1149L0.557225 25.6149L3.44277 28.3851ZM15.4428 13.1149L3.44277 0.614938L0.557223 3.38506L12.5572 15.8851L15.4428 13.1149Z" fill="#1B1B1B"/></svg></div>',
	// 	nextArrow: '<div class="app__slider__arrow__right"><svg width="16" height="29" viewBox="0 0 16 29" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.557225 25.6149C-0.207724 26.4118 -0.181886 27.6778 0.614937 28.4428C1.41176 29.2077 2.67783 29.1819 3.44277 28.3851L0.557225 25.6149ZM14 14.5L15.4428 15.8851C16.1857 15.1111 16.1857 13.8889 15.4428 13.1149L14 14.5ZM3.44277 0.614938C2.67782 -0.181886 1.41176 -0.207723 0.614935 0.557225C-0.181888 1.32218 -0.207726 2.58824 0.557223 3.38506L3.44277 0.614938ZM3.44277 28.3851L15.4428 15.8851L12.5572 13.1149L0.557225 25.6149L3.44277 28.3851ZM15.4428 13.1149L3.44277 0.614938L0.557223 3.38506L12.5572 15.8851L15.4428 13.1149Z" fill="#1B1B1B"/></svg></div>',
	// });
}
