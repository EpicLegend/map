'use strict';
//= ../node_modules/jquery/dist/jquery.js
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js


//= components/stateApp.js
//= collection.js
//= components/toggleMode.js
//= components/slider.js
//= components/dragAndDrop.js


/*
	В каждом компоненте свое пространство имен
	Пример:
		- Файл toggleMode
		- Все переменные(также функции) будут иметь название toggleMode_название переменно
		- Сделано для того чтобы случайно не допустить ошибки в переопределение значения

*/
window.onload = function() {
	// Инициализация начальных настроек
	(function appInit() {

		// Получить состояние с сервера
		app.requestUpdatedApp();

		// биндим кнопки
		toggleMode_init();

		// Постоянно получать состояние с сервера
	})();
};
